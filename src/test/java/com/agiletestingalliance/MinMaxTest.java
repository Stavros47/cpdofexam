package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

	@Test
	public void testFooGreaterA() throws Exception {
		int res = new MinMax().fooBar(10,5);
		assertEquals("Result:",10,res);
	}
	
	@Test
	public void testFooGreaterB() throws Exception {
		int res = new MinMax().fooBar(10,15);
		assertEquals("Result:",15,res);
	}
}
